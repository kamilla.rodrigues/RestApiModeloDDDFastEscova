﻿using RestApiModeloDDD.Application.DTOs;
using System.Linq;

namespace RestApiModeloDDD.Application.Interfaces
{
    public interface IApplicationServiceEmpresa
    {
        void Add(EmpresaDTO empresaDTO);
        void Update(EmpresaDTO empresaDTO);
        void Remove(EmpresaDTO empresaDTO);
        IQueryable<EmpresaDTO> GetAll();
        EmpresaDTO GetById(int id);
    }
}
