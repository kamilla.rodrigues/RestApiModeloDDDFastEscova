﻿using RestApiModeloDDD.Application.DTOs;
using RestApiModeloDDD.Domain.Entities;
using System.Linq;

namespace RestApiModeloDDD.Application.Interfaces.Mapper
{
    public interface IMapperEmpresa
    {
        Empresa MapperDTOToEntity(EmpresaDTO empresaDTO);
        EmpresaDTO MapperEntityToDTO(Empresa empresa);
        IQueryable<EmpresaDTO> MapperListDTOToEntity(IQueryable<Empresa> empresaDTO);
    }
}
