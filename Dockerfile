#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["RestApiModeloDDD.Presentation/RestApiModeloDDD.Presentation.csproj", "RestApiModeloDDD.Presentation/"]
COPY ["RestApiModeloDDD.Infra/RestApiModeloDDD.Infra.csproj", "RestApiModeloDDD.Infra/"]
COPY ["RestApiModeloDDD.Domain.Services/RestApiModeloDDD.Domain.Services.csproj", "RestApiModeloDDD.Domain.Services/"]
COPY ["RestApiModeloDDD.Domain.Core/RestApiModeloDDD.Domain.Core.csproj", "RestApiModeloDDD.Domain.Core/"]
COPY ["RestApiModeloDDD.Domain/RestApiModeloDDD.Domain.csproj", "RestApiModeloDDD.Domain/"]
COPY ["RestApiModeloDDD.Application/RestApiModeloDDD.Application.csproj", "RestApiModeloDDD.Application/"]
RUN dotnet restore "RestApiModeloDDD.Presentation/RestApiModeloDDD.Presentation.csproj"
COPY . .
WORKDIR "/src/RestApiModeloDDD.Presentation"
RUN dotnet build "RestApiModeloDDD.Presentation.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "RestApiModeloDDD.Presentation.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "RestApiModeloDDD.Presentation.dll"]