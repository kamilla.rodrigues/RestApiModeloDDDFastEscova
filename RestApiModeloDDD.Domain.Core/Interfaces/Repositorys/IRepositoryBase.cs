﻿using System.Linq;

namespace RestApiModeloDDD.Domain.Core.Interfaces.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        void Add(T obj);
        void Update(T obj);
        void Remove(T obj);
        IQueryable<T> GetAll();
        T GetById(int id);
    }
}
