﻿using System.Linq;

namespace RestApiModeloDDD.Domain.Core.Interfaces.Services
{
    public interface IServiceBase<T> where T : class
    {
        void Add(T obj);
        void Update(T obj);
        void Remove(T obj);
        IQueryable<T> GetAll();
        T GetById(int id);
    }
}
